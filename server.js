var http = require("http"),
	url = require("url"),
	config = require("./config");

function start(route, handle) {
	function onRequest(request, response) {
		var pathname = url.parse(request.url).pathname;
		console.log("Request for " + pathname + " received.");
		route(handle, pathname, response, request);
	}
	http.createServer(onRequest).listen(config.server_port);
	console.log("Server has started on port "+config.server_port);
}
exports.start = start;

